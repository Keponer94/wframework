# Witrac Framework  #

Controller and basic html estructures for PHP

## Getting Started



### Prerequisites

To use this framework you need 

```
* MySQL Server
* Server with PHP 5.0 or greater
* Apache
```

### Installing

To use all the framwork (HTML + Controller)

```
* Create a folder in your main web folder called "WFramework"
* Put include_once("/WFramework/WFrameworkHTML.php");
```

To use only controller 

```
* Create a folder in your main web folder called "WFramework"
* Put include_once("/WFramework/WFrameworkDB.php");
```



* * *



# Functions



## WFrameworkDB



### executeQuery($query)

> Execute $query, if query have been executed return true, else false
> > Type: Any kind of query (String)




```
$query = executeQuery("UPDATE users SET active = 0 WHERE id = 1")

Return: $query == True
```

### getColNames($query)

> Return the Names of the cols in the query, ordered like the query and begin from 0. format array[number of col]
> > Type: Select Query (String)


```
$ColNames = getColNames("SELECT * FROM user")

Return: $ColNames[0] == "Name"
        $ColNames[1] == "Surnames"
        $ColNames[2] == "UserID"
              .
              .
              .
```